import React from 'react';
import Login from './containers/Login';
import App from './containers/App';
import {Route} from 'react-router';

export const routes = (
  <div>
    <Route path="/app" component={App}/>
    <Route path="/" component={Login}/>
  </div>
);