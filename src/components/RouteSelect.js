import React, {PropTypes, Component} from 'react';
import ReactDOM from 'react-dom';
import {FormControl} from 'react-bootstrap';

export default class RouteSelect extends Component {
  componentDidMount() {
    let routesElement = ReactDOM.findDOMNode(this.select);
    let tripsElement = ReactDOM.findDOMNode(this.refs.trips);
    this.props.getRoutes(routesElement, tripsElement);
  }
  handleTripSelect(event) {
    event.preventDefault();
    let activeElement = ReactDOM.findDOMNode(this.refs.trips).getElementsByClassName('active')[0];
    if(typeof activeElement != 'undefined') {
      activeElement.classList.remove('active');
    }
    event.target.parentElement.classList.add('active');
    this.props.selectTrip(event.target.getAttribute('data-id'));
  }
  handleRouteSelect(event) {
    let tripsElement = ReactDOM.findDOMNode(this.refs.trips);
    this.props.changeRoute(this.props.trips, event.target.value, tripsElement);
  }
  render() {
    return (
      <div>
        <FormControl className="bottom-space-10" componentClass="select" ref={select => { this.select = select }} onChange={::this.handleRouteSelect}>
          <option value="0">Данные недоступны</option>
        </FormControl>
        <ul className="nav nav-pills nav-stacked" onClick={::this.handleTripSelect} ref="trips">
        </ul>
      </div>
    );
  }
}

RouteSelect.propTypes = {
  getRoutes: PropTypes.func.isRequired,
  changeRoute: PropTypes.func.isRequired,
  selectTrip: PropTypes.func.isRequired,
  trips: PropTypes.object.isRequired
};