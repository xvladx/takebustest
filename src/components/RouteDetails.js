import React, {PropTypes, Component} from 'react';
import ReactDOM from 'react-dom';
import {Panel, Col, Button} from 'react-bootstrap';
import {TRIP_STATUSES} from '../constants/Trips';
import {getDatetime} from '../actions/RouteActions';

export default class RouteDetails extends Component {
  handleSubmit(e) {
    e.preventDefault();
    // console.log(e.target);
    let adultPrice = ReactDOM.findDOMNode(this.refs.adultPrice).value;
    let childPrice = ReactDOM.findDOMNode(this.refs.childPrice).value;
    let {departureDateTime, state, totalSeats, platform, driver, bus} = this.props.trip;
    let seats = [];
    for(let i=1;i<=totalSeats;i++) {
      seats.push(`${i}`);
    }
    this.props.updateTrip(this.props.trip.id, {
      childPrice: childPrice,
      price: adultPrice,
      departure: departureDateTime,
      state: state,
      seats: seats,
      departurePlatform: platform,
      driverId: driver.id,
      busId: bus.id
    });
  }
  render() {
    let {bus, state, driver, platform, price, childPrice} = this.props.trip;
    let time = {
      departure: getDatetime(this.props.trip.departureDateTime),
      arrival: getDatetime(this.props.trip.arrivalDateTime)
    };
    console.log(price, childPrice);
    return (
      <div>
        <Panel header="Детали рейса">
          <Col md={6} mdPush={6}> {/*Вторая колонка*/}
            <p>Время отправления: <strong>{time.departure.hours}:{time.departure.minutes}</strong></p>
            <p>Рассчетное время прибытия: <strong>{time.arrival.hours}:{time.arrival.minutes}</strong></p>
            <p>Платформа: <strong>{platform}</strong></p>
          </Col>
          <Col md={6} mdPull={6}>
            <p>Автобус: <strong>{bus.model}</strong></p>
            <p>Статус рейса: <strong>{TRIP_STATUSES[state-1]}</strong></p>
            <p>Водитель: <strong>{driver.lastName} {driver.firstName} {driver.middleName}</strong></p>
            <form onSubmit={::this.handleSubmit}>
              <div className="form-group">
                <label className="control-label">Взрослый</label>
                <input ref="adultPrice" placeholder="Введите значение" className="form-control" key={price} defaultValue={price} type="text" />
              </div>
              <div className="form-group">
                <label className="control-label">Детский</label>
                <input ref="childPrice" placeholder="Введите значение" className="form-control" key={childPrice} defaultValue={childPrice} type="text" />
              </div>
              <Button bsStyle="success" type="submit">Сохранить</Button>
            </form>
          </Col>
        </Panel>
      </div>
    );
  }
}

RouteDetails.propTypes = {
  trip: PropTypes.object.isRequired,
  updateTrip: PropTypes.func.isRequired
};