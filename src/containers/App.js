import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Col, Button, Panel} from 'react-bootstrap';
import {bindActionCreators} from 'redux';
import {ROUTING} from '../constants/Routing';
import RouteSelect from '../components/RouteSelect';
import RouteDetails from '../components/RouteDetails';
import * as routeActions from '../actions/RouteActions';
import * as tripActions from '../actions/TripsActions';
import * as userActions from '../actions/UserActions';
import Loading from 'react-loading-animation';

class App extends Component {
  componentDidMount() {
    this.checkAuth(this.props.user);
  }
  checkAuth(user) {
    if (!user.username) {
      this.props.dispatch({
        type: ROUTING,
        payload: {
          method: 'replace',
          nextUrl: '/'
        }
      })
    }
    else if (!user.isAuthenticated) {
      this.props.userActions.login(user, true);
    }
  }
  logOut() {
    this.props.userActions.logout();
  }
  render() {
    let user = this.props.user;
    let template = <Loading />;
    const {getRoutes, changeRoute, selectTrip} = this.props.routeActions;
    const {updateTrip} = this.props.tripActions;
    const {trips, trip} = this.props.trips;
    if(user.isAuthenticated) {
      template = (
        <div>
          <Panel header="TakeBus">
            <Col xs={12} md={4} sm={4} className="bottom-space-20">
              <RouteSelect
                getRoutes={getRoutes}
                trips={trips}
                changeRoute={changeRoute}
                selectTrip={selectTrip}
              />
            </Col>
            <Col xs={12} md={8} sm={8}>
              <RouteDetails
                trip={trip}
                updateTrip={updateTrip}
              />
              <Button bsStyle="danger" onClick={::this.logOut}>Выйти</Button>
            </Col>
          </Panel>
        </div>
      );
    }
    return (
      <div>
        {template}
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    routeActions: bindActionCreators(routeActions, dispatch),
    tripActions: bindActionCreators(tripActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    page: state.page,
    trips: state.trips,
    trip: state.trip
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);