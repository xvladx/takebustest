import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActions from '../actions/UserActions';
import Loading from 'react-loading-animation';

class Login extends Component {
  componentDidMount() {
    this.checkAuth(this.props.user);
  }
  checkAuth(user) {
    if (user.username&&!user.isAuthenticated) {
      this.props.actions.login(user);
    }
  }
  handleSubmit(e) {
    e.preventDefault();
    let username = ReactDOM.findDOMNode(this.refs.login).value;
    let password = ReactDOM.findDOMNode(this.refs.password).value;
    this.props.actions.login({username: username, password: password, isAuthenticated: true})
  }
  render() {
    let template = (
      <div className="loginmodal-container">
        <h1>Авторизация</h1><br />
        <form onSubmit={::this.handleSubmit}>
          <input type="text" name="user" ref="login" placeholder="Имя пользователя" />
          <input type="password" name="pass" ref="password" placeholder="Пароль" />
          <input type="submit" name="login" className="login loginmodal-submit" value="Войти" />
        </form>
      </div>
    );
    if(this.props.user.signingIn==true||this.props.user.username&&!this.props.user.isAuthenticated) {
      template = <Loading />;
    }
    return (
      <div className="modal-dialog">
        {template}
      </div>
    );
  }
}



function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);