export const TRIP_STATUSES = [
  'По расписанию',
  'Отменен',
  'Нет мест',
  'Отправлен',
  'Продажа закрыта'
];
export const TRIP_UPDATE_REQUEST = 'TRIP_UPDATE_REQUEST';
export const TRIP_UPDATE_SUCCESS = 'TRIP_UPDATE_SUCCESS';