export const GET_ROUTES_REQUEST = 'GET_ROUTES_REQUEST';
export const GET_ROUTES_SUCCESS = 'GET_ROUTES_SUCCESS';
export const CHANGE_ROUTE = 'CHANGE_ROUTE';
export const SELECT_TRIP_REQUEST = 'SELECT_TRIP_REQUEST';
export const SELECT_TRIP_SUCCESS = 'SELECT_TRIP_SUCCESS';