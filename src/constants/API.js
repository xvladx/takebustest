export const API_BASE_URL = 'http://tb-api-test.azurewebsites.net/api/v1/';
export const CARRIER_ROUTE_GET_LIST = 'carrier/route/get/list';
export const CARRIER_INSTANCE_GET_LIST = 'carrier/instance/get/list';
export const CARRIER_INSTANCE_GET_BY_ID = 'carrier/instance/get/:id';
export const CARRIER_INSTANCE_UPDATE = 'carrier/instance/update';