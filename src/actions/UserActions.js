import {
  LOGIN_REQUEST,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  TOKEN_URL
} from '../constants/User';
import {
  ROUTING
} from '../constants/Routing';
import axios from 'axios';
import querystring from 'querystring';

export function login(payload, noRedirect = false) {
  return (dispatch) => {

    dispatch({
      type: LOGIN_REQUEST
    });

    const config = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    };

    //Token URL
    axios.post(TOKEN_URL, querystring.stringify({
      username: payload.username,
      password: payload.password,
      grant_type: 'password'
    }), config)
      .then(function (response) {
        window.localStorage.setItem('tb_user', JSON.stringify({
          access_token: response.data.access_token,
          username: payload.username,
          password: payload.password
        }));

        if(noRedirect==true) {
          console.log('Login success!');
          return dispatch({
            type: LOGIN_SUCCESS,
            payload: {
              username: payload.username,
              password: payload.password,
              access_token: response.data.access_token,
              isAuthenticated: true
            }
          });
        }
        dispatch({
          type: LOGIN_SUCCESS,
          payload: {
            username: payload.username,
            password: payload.password,
            access_token: response.data.access_token,
            isAuthenticated: true
          }
        });
        return dispatch({
          type: ROUTING,
          payload: {
            method: 'replace',
            nextUrl: '/app'
          }
        });
      })
      .catch(function (error) {
        window.localStorage.removeItem('tb_user');
        console.log(error);
        return dispatch({
          type: LOGIN_FAIL
        });
      });
  }
}

export function logout() {
  return (dispatch) => {
    window.localStorage.removeItem('tb_user');
    dispatch({
      type: LOGOUT_SUCCESS
    });
    return dispatch({
      type: ROUTING,
      payload: {
        method: 'replace',
        nextUrl: '/'
      }
    });
  }
}