import {
  TRIP_UPDATE_REQUEST,
  TRIP_UPDATE_SUCCESS
} from '../constants/Trips';
import {
  API_BASE_URL,
  CARRIER_INSTANCE_UPDATE,
} from '../constants/API';
import axios from 'axios';

export function updateTrip(tripId, data) {
  return (dispatch, getState) => {
    dispatch({
      type: TRIP_UPDATE_REQUEST
    });
    let {access_token} = getState().user;
    const config = {
      headers: { Authorization: `Bearer ${access_token}` }
    };

    data.id = tripId;
    return axios.post(API_BASE_URL+CARRIER_INSTANCE_UPDATE, data, config)
      .then(response => {
        if(response.data.success!==true) {
          throw new Error(response.data.errors[0].description);
        }
        let viewModel = response.data.viewModel;
        return dispatch({
          type: TRIP_UPDATE_SUCCESS,
          payload: viewModel
        });
      })
      .catch(error => {
        console.log(`error while getting ${CARRIER_INSTANCE_UPDATE}`, error);
      })
  }
}