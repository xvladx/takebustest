import {
  GET_ROUTES_REQUEST,
  GET_ROUTES_SUCCESS,
  CHANGE_ROUTE,
  SELECT_TRIP_REQUEST,
  SELECT_TRIP_SUCCESS
} from '../constants/Routes';
import {
  API_BASE_URL,
  CARRIER_INSTANCE_GET_LIST,
  CARRIER_INSTANCE_GET_BY_ID
} from '../constants/API';
import axios from 'axios';

export function getRoutes(routesElement = null, tripsElement = null) {
  return (dispatch, getState) => {
    dispatch({
      type: GET_ROUTES_REQUEST
    });
    let {access_token} = getState().user;
    const config = {
      headers: { Authorization: `Bearer ${access_token}` }
    };
    return axios.get(API_BASE_URL+CARRIER_INSTANCE_GET_LIST, config)
      .then(response => {
        if(response.data.success!==true) {
          throw new Error(response.data.errors);
        }
        let viewModel = response.data.viewModel;
        let trips = {};
        for (let i in viewModel) {
          let route_id = viewModel[i].route.id;
          if(typeof trips[route_id] == 'undefined') {
            trips[route_id] = {
              objects: [],
              name: viewModel[i].route.name
            };
          }
          trips[route_id].objects.push(viewModel[i]);
        }
        setRoutes(trips, routesElement);
        setTrips(trips[Object.keys(trips)[0]].objects, tripsElement, trips[Object.keys(trips)[0]].name);
        dispatch({
          type: GET_ROUTES_SUCCESS,
          payload: trips
        });
        return selectTrip(trips[Object.keys(trips)[0]].objects[0].id)(dispatch, getState);
      })
      .catch(error => {
        console.log(`error while getting ${CARRIER_INSTANCE_GET_LIST}`, error);
      })
  }
}

function setRoutes(routes, element) {
  if(element==null) {
    return false;
  }
  for(let i = 0; i<element.options.length; i++) {
    element.options[i] = null;
  }
  for(let i in routes) {
    element.options.add(new Option(routes[i].name, i));
  }
  return element;
}

function setTrips(trips, element, name) {
  if(element == null) {
    return false;
  }
  let elements = '';
  for(let i in trips) {
    let departure = getDatetime(trips[i].departureDateTime);
    let arrival = getDatetime(trips[i].arrivalDateTime);
    let className = (i==0)? ' class="active"' : '';
    elements+= `<li${className}>
<a href="#" data-id="${trips[i].id}">${name} - ${departure.hours}:${departure.minutes} - ${arrival.hours}:${arrival.minutes}</a>
</li>`;
  }
  element.innerHTML = elements;

  return trips;
}

export function getDatetime(timeStr) {
  if(timeStr==null) {
    return {
      year: '',
      month: '',
      day: '',

      hours: '',
      minutes: '',
      seconds: ''
    };
  }
  let dateTime = timeStr.split('T');
  let date = dateTime[0].split('-');
  let time = dateTime[1].split('+')[0].split(':');
  return {
    year: date[0],
    month: date[1],
    day: date[2],

    hours: time[0],
    minutes: time[1],
    seconds: time[2]
  };
}

export function changeRoute(routes, route_id, tripsElement = null) {
  return (dispatch, getState) => {
    setTrips(routes[route_id].objects, tripsElement, routes[route_id].name);

    dispatch({
      type: CHANGE_ROUTE
    });

    return selectTrip(routes[route_id].objects[0].id)(dispatch, getState);
  }
}


export function selectTrip(trip_id) {
  return (dispatch, getState) => {
    dispatch({
      type: SELECT_TRIP_REQUEST
    });
    let {access_token} = getState().user;
    const config = {
      headers: { Authorization: `Bearer ${access_token}` }
    };
    return axios.get(API_BASE_URL+CARRIER_INSTANCE_GET_BY_ID.replace(':id',trip_id), config)
      .then(response => {
        if(response.data.success!==true) {
          throw new Error(response.data.errors[0].description);
        }
        return dispatch({
          type: SELECT_TRIP_SUCCESS,
          payload: response.data.viewModel
        });
      })
      .catch(error => {
        console.log(`error while getting ${CARRIER_INSTANCE_GET_BY_ID.replace(':id',trip_id)}`, error);
      })
  }
}