import {
  GET_ROUTES_SUCCESS,
  CHANGE_ROUTE,
  SELECT_TRIP_SUCCESS,
  SELECT_TRIP_REQUEST
} from '../constants/Routes';
import {
  TRIP_UPDATE_REQUEST,
  TRIP_UPDATE_SUCCESS
} from '../constants/Trips';
const initialState = {
  trips: {},
  trip: {
    bus: {
      model: ''
    },
    driver: {
      lastName: '',
      firstName: '',
      middleName: ''
    },
    platform: '',
    price: 0,
    childPrice: 0,
    departureDateTime: null,
    arrivalDateTime: null,
    state: 1
  }
};

export default function trips(state = initialState, action) {

  switch (action.type) {
    case GET_ROUTES_SUCCESS:
      return {
        ...state,
        trips: action.payload
      };
    case CHANGE_ROUTE:
      return state;
    case SELECT_TRIP_REQUEST:
      return state;
    case SELECT_TRIP_SUCCESS:
      return {
        ...state,
        trip: action.payload
      };
    case TRIP_UPDATE_REQUEST:
      return state;
    case TRIP_UPDATE_SUCCESS:
      return {
        ...state,
        trip: action.payload
      };
    default:
      return state;
  }
}