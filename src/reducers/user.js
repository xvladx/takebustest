import {
  LOGIN_REQUEST,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS
} from '../constants/User';

console.log(window.localStorage.getItem('tb_user'));
const initialState = {
  ...JSON.parse(window.localStorage.getItem('tb_user')),
    signingIn: false
} || {
  signingIn: false
  };

export default function user(state = initialState, action) {

  switch (action.type) {

    case LOGIN_REQUEST:
      return {
        ...state,
        signingIn: true
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        username: action.payload.username,
        password: action.payload.password,
        access_token: action.payload.access_token,
        isAuthenticated: action.payload.isAuthenticated,
        signingIn: false
      };

    case LOGIN_FAIL:
      return {};

    case LOGOUT_SUCCESS:
      return {};

    default:
      return state
  }
}